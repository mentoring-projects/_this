function Item(container, delegate)
{
    var _this = {};
    
    _this.container = container;
    _this.delegate = delegate;
    _this.id = null;
    
    
    this.init = function()
    {
        _this.id = _this.container.attr("data-id");
        
        _this.container.click(function(){
            _this.delegate.clicked(_this.id);
        });
    };
}