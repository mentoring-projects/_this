function List(container, delegate)
{
    var _this = {};
    
    _this.container = container;
    _this.delegate = delegate;
    _this.items = [];
    
    this.init = function()
    {
        _this.container.find("[data-item]").each(function(index, value){
            var $item = $(value);

            var item = new Item(
                $item,
                {
                    clicked: _this.itemClicked
                }
            );
            item.init();
            
            _this.items.push(item);
        });
    };
    
    _this.itemClicked = function(itemID)
    {
        _this.delegate.itemClicked(itemID);
    }
}