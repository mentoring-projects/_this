function Filter(container, delegate)
{
    var _this = {};
    
    _this.container = container
    _this.delegate = delegate;
    _this.list = null;
    
    
    this.init = function()
    {
        _this.list = new List(
            _this.container.find("[data-list]"),
            {
                itemClicked: _this.listItemClicked
            }
        );
        _this.list.init();
    };
    
    _this.listItemClicked = function(itemID)
    {
        _this.delegate.itemClicked(itemID);
    };
}