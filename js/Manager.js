function Manager(container)
{
    var _this = {};
    
    _this.container = container;
    _this.map = null;
    _this.filter = null;
    
    
    this.init = function()
    {
        console.log(container);
        _this.map = new Map(_this.container.find("[map-container]"));
        _this.filter = new Filter(
            _this.container.find("[filter-container]"),
            {
                itemClicked: _this.filterItemClicked
            }
        );
        
        _this.map.init();
        _this.filter.init();
    };
    
    _this.filterItemClicked = function(itemID)
    {
        _this.map.showItem(itemID);
    };
}