function Map(container)
{
    var _this = {};
    
    _this.container = container
    _this.map = null;
    _this.markers = {};
    
    
    this.init = function()
    {
        _this.map = new google.maps.Map(
            _this.container[0],
            {
                center: {lat: -34.397, lng: 150.644},
                zoom: 8
            }
        );
        
        _this.markers[1] = new google.maps.Marker({
          position: new google.maps.LatLng(48, 18),
          map: _this.map,
          title: 'Project 1'
        });
    };
    
    this.showItem = function(itemID)
    {
        _this.markers[1].setVisible(Math.random() > 0.5);
    };
}